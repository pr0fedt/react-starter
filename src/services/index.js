'use strict';

import Http from 'immutable-http';

const FileRequest = (new Http()).withMethod('GET');
const ConfigRequest = (new Http()).withMethod('GET')
	.withUrl('/public/json/config.:name.json')
	.withResponseType('json');

export function getFileService(action){
    return FileRequest.withUrl(action.path).exec();
}
export function getConfigService(action){
	return ConfigRequest.withDynamicSegment('name', action.name).exec();
}