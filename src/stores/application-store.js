'use strict';
import _ from 'lodash';
import {ReactStore} from 'flux-extended';
import moment from 'moment';

import invariant from '../lib/invariant';

import {
	E_CHANGE_PAGE_TITLE,
	E_CONFIG_REQUEST, E_CONFIG_SUCCESS, 
	E_LOG_ACTION, E_START_APP
} from '../constants';

const applicationStore = new ReactStore({}, {extend: {
	getTitle(){
		return this._data['title'];
	},

	getLog(){
		return this._data['log'];
	},

	isReady(){
		return new Promise((resolve, reject) => {
			if(this._data['ready']){
				resolve();
			}else {
				this.on('ready', () => {
					this.off('ready');
					this._data['ready'] = moment();
					resolve();
				});
			}
		});
	},

	isStarted(){
		return !!this._data['start'];
	},

	nextModule(){
		invariant(this.isStarted(), 'application not started');
		let mtl = this._data['modulesToLoad'];
		return mtl.length > 0 ? mtl[0] : null;
	}
}});

applicationStore.setHandler(E_CHANGE_PAGE_TITLE, function onPageTitleChange(payload){
	invariant(this.isReady(), 'modules are not loaded');
	let {title} = payload.action;
	this._data['title'] = title;
	this._emit();
});

applicationStore.setHandler(E_CONFIG_REQUEST, function onConfigRequested(){
	invariant(this.isStarted(), 'application not started');
});

applicationStore.setHandler(E_CONFIG_SUCCESS, function onConfigLoaded(payload){
	let {name} = payload.action;
	this._data['modulesToLoad'] = _.filter(this._data['modulesToLoad'], moduleName => 
		moduleName != name
	);
	this._emit();
});

applicationStore.setHandler(E_START_APP, function onApplicationStart(payload){
	invariant(payload.action, 'no data was passed to action');
	invariant(payload.action.modules, 'no modules to load');
	this._data['modulesToLoad'] = payload.action.modules;
	this._data['start'] = moment();
	this._emit();
});

applicationStore.setHandler(E_LOG_ACTION, function onActionLog(payload){
	if(!this._data['log']){
		this._data['log'] = [];
	}
	this._data['log'].unshift(_.assign({now: moment()}, payload.action));
	this._emit();
});


export {applicationStore};