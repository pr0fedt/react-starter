'use strict';
import _ from 'lodash';
import {ReactStore} from 'flux-extended';
import moment from 'moment';

import invariant from '../lib/invariant';

import {E_CONFIG_SUCCESS} from '../constants';

const configurationStore = new ReactStore({}, {extend: {
	getConfiguration(name){
		invariant(this._data['config'], 'config is not initialized');
		let config = this._data['config'][name];
		invariant(config, `config[${name}] was not loaded`)
		return config;
	},
	getCurrent(){
		invariant(this._data['current'], 'current config not set');
		let current = this._data['current'];
		this._data['current'] = null;
		return current;
	}
}});

configurationStore.setHandler(E_CONFIG_SUCCESS, function onConfigLoad(payload){
	if(!_.isObject(this._data['config'])){
		this._data['config'] = {};
	}

	let {name} = payload.action;
	this._data['current'] = name;
	this._data['config'][name] = payload.data.response;
	this._emit();
});

export {configurationStore};