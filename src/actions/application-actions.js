'use strict';
import {ActionCreator} from 'flux-extended';
import {getConfigService} from '../services';
import {
	E_CHANGE_PAGE_TITLE, E_LOG_ACTION, 
	E_CONFIG_REQUEST, E_CONFIG_SUCCESS, E_CONFIG_FAIL,
	E_START_APP, E_READY_APP
} from '../constants';

const applicationActionCreator = new ActionCreator();
applicationActionCreator.createAction('setPageTitle', E_CHANGE_PAGE_TITLE);
applicationActionCreator.createAction('startApplication', E_START_APP);
applicationActionCreator.createAction('logAction', E_LOG_ACTION);
applicationActionCreator.createAction('requestConfig', E_CONFIG_REQUEST)
	.withService(getConfigService).withServiceSource('resource')
	.withTypes(E_CONFIG_SUCCESS, E_CONFIG_FAIL);
applicationActionCreator.createAction('applicationReady', E_READY_APP);

export {applicationActionCreator as applicationActions};