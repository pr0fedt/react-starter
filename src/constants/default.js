'use strict';
export const BODY = {
	CLASSNAME: 'application-body'
};
export const CONFIG = 'application';
export const LANGUAGE = 'en';
export const TITLE = 'page title';
