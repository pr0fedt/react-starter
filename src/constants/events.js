'use strict';
export const E_CHANGE_PAGE_TITLE = 'changePageTitle';
export const E_CONFIG_REQUEST = 'requestConfig';
export const E_CONFIG_SUCCESS = 'requestConfigSuccess';
export const E_CONFIG_FAIL = 'requestConfigError';
export const E_LOG_ACTION = 'logAction';
export const E_START_APP = 'Application:Start';
export const E_READY_APP = 'Application:Ready';