'use strict';
import React from 'react';
import {Router, Route} from 'react-router';

import {App} from '../../components';
import {NotFound} from '../../components/Page';

export function createRoutes(history){
	return (
		<Router history={history}>
		    <Route path="/" component={App} >
		        <Route path="*" component={NotFound} />
		    </Route>
		</Router>
	);	
} 