/**
 * es6 invariant assertion 
 */

'use strict';

export default function invariant(condition, ...invariantMessageParts){
  if(!invariantMessageParts){
    throw new Error('invariant requires at least one message part');
  }
  if(!condition){
    let error = new Error(`Invariant violation: ${invariantMessageParts.join(' ')}`);
    error.framesToPop = 1;
    throw error;
  }
  return true;
}
