'use strict';

import {ReactStore} from 'flux-extended';
import {E_FILE_REQUEST, E_FILE_SUCCESS} from '../../../constants';

let fileStore = new ReactStore({}, { extend: {
	isLoaded() {
		return !!this._data['loaded'];
	},
	getFile() {
		return this._data['content'];
	}
}});

fileStore.setHandler(E_FILE_REQUEST, function onFileRequested(payload){
	this._data['loaded'] = false;
	this._emit();
});

fileStore.setHandler(E_FILE_SUCCESS, function onFileReceived(payload){
	this._data['loaded'] = true;
	this._data['content'] = payload.data.response;
	this._emit();
});

export {fileStore};