'use strict';

import React from 'react';

import {Container, Loading, MarkdownText} from '../../Elements';
//import MarkDownText from './markdown-text';

import {appActionCreator} from '../../../actions';
import {homeActionCreator} from './actions';
import {fileStore} from './stores';

const Home = React.createClass({
    mixins: [ fileStore.getMixin('file') ],
    getInitialState(){
        let isLoaded = this.fileStore.isLoaded();
        return {
            isLoaded
        };
    },
    componentDidMount(){
        homeActionCreator.requestFile({
            path: '/public/res/README.md'
        });
        appActionCreator.setPageTitle({
            title: 'Home'
        });
    },
    fileStoreChanged(){
        let isLoaded = this.fileStore.isLoaded();
        if(isLoaded){
            let content = this.fileStore.getFile();
            this.setState({isLoaded, content});
        }
    },
    render(){
        return (
            <Container>
            {this.state.isLoaded ? (
                <MarkdownText>
                    {this.state.content}
                </MarkdownText>
            ) : (
                <Loading />
            )}
            </Container>
        );
    }
});

export {Home};
