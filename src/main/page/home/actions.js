'use strict';

import {ActionCreator} from 'flux-extended';
import {getFileService} from '../../../services'
import {E_FILE_REQUEST, E_FILE_SUCCESS, E_FILE_FAIL} from './constants';

const homeActionCreator = new ActionCreator();
homeActionCreator.createAction('requestFile', E_FILE_REQUEST)
    .withService(getFileService).withTypes(E_FILE_SUCCESS, E_FILE_FAIL);

export {homeActionCreator};