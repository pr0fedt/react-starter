'use strict';
export const E_FILE_REQUEST = 'requestFile';
export const E_FILE_SUCCESS = 'requestFileSuccess';
export const E_FILE_FAIL = 'requestFileError';