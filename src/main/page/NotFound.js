'use strict';
import _ from 'lodash';
import React from 'react';
import {Alert, PageHeader} from 'react-bootstrap';
import {Container} from '../Elements';

import {appActionCreator} from '../../actions';
const NotFound = React.createClass({
	getInitialState(){
		let {location, params, routeParams} = this.props;
		return {location, params, routeParams};
	},
	componentDidMount(){
		appActionCreator.setPageTitle({title: 'Page not found'});
	},
	componentWillReceiveProps(nextProps){
		let {location, params, routeParams} = nextProps;
		this.setState({location, params, routeParams});
	},
	render(){
		return (
			<Container>
				<PageHeader>
					Error 404
				</PageHeader>
				<Alert bsStyle="danger">
					<h4>Page not found</h4>
					{_.map(this.state, (param, key) => (
						<pre key={key}>
							{key}:
							{JSON.stringify(param, null, 2)}
						</pre>
					))}
				</Alert>
			</Container>
		);
	}
});
export {NotFound};