var path        = require('path'),
    _           = require('lodash'),
    React       = require('react'),
    RBootstrap  = require('react-bootstrap'),
    Bootstrap   = require('../bootstrap');

var navigationStore   = require('../application/navigation').store,
    navigationActions = require('../application/navigation').actions,
    constants         = require('../../constants');

var Paging = React.createClass({
    contextTypes: {
        router: React.PropTypes.func.isRequired
    },
    getInitialState: function(){
        navigationStore.fromContext(this.context.router);
        var routerInfo = navigationStore.getInfo();
        return {
            total: 10,
            current: routerInfo.params.num
        };
    },
    componentDidMount: function () {
        navigationStore.register(constants.NAVIGATION.EVENTS.DATA, this.onURIData);
    },
    componentWillUnmount: function () {
        navigationStore.unregister(constants.NAVIGATION.EVENTS.DATA, this.onURIData);
    },
    componentWillReceiveProps: function(){
        navigationStore.fromContext(this.context.router);
    },
    onURIData: function(data){
        this.setState({
            current: data.params.num
        })
    },
    selectPage: function(num){
        navigationActions.changeAddress.exec({
            context : this.context.router,
            to      : 'paging',
            params  : {num: num}
        });
    },
    render: function(){
        return (
            <Bootstrap.Container>
                <RBootstrap.ButtonGroup>
                {_.times(this.state.total, function(i){
                    return (
                        <RBootstrap.Button key={i} href={'#/' + (i+1)}>{i + 1}</RBootstrap.Button>
                    );
                })}
                </RBootstrap.ButtonGroup>
                <p>
                    <a href="javascript:;"
                        onClick={_.partial(this.selectPage, this.state.total)}>
                            To Last Page
                    </a>
                </p>
                <p>
                    Selected page: {this.state.current}
                </p>
            </Bootstrap.Container>
        );
    }
});

module.exports = Paging;
