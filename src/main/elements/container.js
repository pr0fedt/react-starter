'use strict';
import React from 'react';

const Container = React.createClass({
    render() {
        return (
            <div className={this.props.isFluid ? 'container-fluid' : 'container'}>
                {this.props.children}
            </div>
        );
    }
});

export {Container};
