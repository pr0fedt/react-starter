'use strict';

import React from 'react';
import {Panel, ProgressBar} from 'react-bootstrap';

const Loading = React.createClass({
	getInitialState(){
		let now = this.props.now ? this.props.now : 100;
		return {
			now
		};
	},
	componentWillReceiveProps(nextProps){
		let now = nextProps.now ? nextProps.now : 100;
		this.setState({
			now
		});
	},
	render(){
		return (
			<Panel bsStyle="primary">
				<ProgressBar active now={this.state.now} 
					style={{
						marginBottom: 0
					}}/>
			</Panel>
		);
	}
});

export {Loading};