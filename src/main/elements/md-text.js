var
/* ReactJS Library */
    React     = require('react'),
/* 3RD PARTY MODULES */
    _         = require('lodash'),
    highlight = require('highlight.js'),
    marked    = require('marked'),
/* Element Declaration */
    MarkdownText,
/* CONSTANTS */
    HIGHLIGHT_OPTIONS = {
        METHODS: {
            AUTO: 'highlightAuto',
            MANUAL: 'highlight'
        },
        LANGUAGES: highlight.listLanguages()
    };

marked.setOptions({
    breaks: false,
    gfm: true,
    pedantic: false,
    sanitize: true,
    smartLists: true,
    smartypants: false,
    tables: true,

    highlight: function(code, lang){
        if(_.isString(lang)){
            return highlight.highlight(lang, code).value;
        }
        return highlight.hihglightAuto(code).value;
    },
    renderer: new marked.Renderer(),
});

MarkdownText = React.createClass({
    render: function() {
        let textNode  = this.props.children || "";
        let rawMarkup = marked(textNode.toString());
        return (
            <div className="markdown-text"
                dangerouslySetInnerHTML={{__html: rawMarkup}}>
            </div>
        );
    }
});

export {MarkdownText};
