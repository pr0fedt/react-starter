'use strict';
import _ from 'lodash';
import React from 'react';
import DocMeta from 'react-doc-meta';
import SideEffect from 'react-side-effect';

const DocTitle = SideEffect(function(propsList){
    var elemProps = propsList.slice(-1)[0];
    if(!_.isUndefined(document)){
        document.title = elemProps.title || '';
    }
}, {
    displayName: 'DocTitle',
    propTypes: {
        title: React.PropTypes.string.isRequired
    }
});

const AppHead = React.createClass({
    getInitialState(){
        return {
            metaTags: this.props.metaTags || [],
            title: this.props.docTitle
        };
    },
    componentWillReceiveProps(nextProps){
        let title = nextProps.docTitle;
        this.setState({title});
    },
    render: function(){
        return (
            <div style={{display: 'none'}}>
                <DocMeta tags={this.state.metaTags} />
                <DocTitle title={this.state.title}  />
            </div>
        );
    }
});

export default AppHead;