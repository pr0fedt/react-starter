'use strict';

import _ from 'lodash';
import classnames from 'classnames';
import React from 'react';
import {BODY}from '../../../constants';
import AppMenu from './menu';

const AppBody = React.createClass({
    getInitialState(){
        return {
            hideMenu: this.props.hideMenu || false,
            fixed: 'top'
        };
    },
    render() {
        let opts = {
            className: BODY.CLASSNAME,
            brand : this.props.brand || '',
            menu  : this.props.menu  || []
        };
        if(this.props.logging){
            let debug = (_.get(this.props.location, 'query.debug') != 'show') ? 'show' : 'hide';
            opts.menu['debug'] = {
                href: this.props.location.pathname,
                query: _.assign({}, this.props.location.query, {debug}),
                title: '[DBG]'
            };
        }
        let fixedTop = this.state.fixed == 'top';
        let fixedBottom = this.state.fixed == 'bottom';
        return (

            <div className={classnames(
                opts.className, {
                    'has-nav-fixed-top': fixedTop,
                    'has-nav-fixed-bottom': fixedBottom
                })}>
                {this.state.hideMenu ? '' : (
                    <AppMenu brand={opts.brand} 
                        fixedTop={this.state.fixed == 'top'}
                        menu={opts.menu}/>
                )}
                <section className="application-body-content">
                    {this.props.children}
                </section>
            </div>
        );
    }
});

export default AppBody;
