'use strict';
import _ from 'lodash';
import React from 'react';
import {Navbar, Nav, NavItem} from 'react-bootstrap';

import {Link} from 'react-router';
const Menu = React.createClass({
    render(){
        return (
            <Navbar brand={this.props.brand} 
                fixedTop={this.props.fixedTop}
                fixedBottom={this.props.fixedBottom}
                fluid={this.props.fluid}
                inverse={this.props.inverse}>
                <Nav>
                    {_.map(this.props.menu, (val, key) => (
                        <NavItem key={key} href="#">
                            <Link to={val.href} query={val.query} className="menu-link-fix">
                                {val.title}
                            </Link>
                        </NavItem>
                     )
                    )}
                </Nav>
            </Navbar>
        );
    }
});

export default Menu;
