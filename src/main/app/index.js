'use strict';
import _ from 'lodash';
import classnames from 'classnames';
import React from 'react';
import {Panel} from 'react-bootstrap';
import {RouteHandler} from 'react-router';

import {CONFIG, LANGUAGE, TITLE} from '../../constants';
import {appActionCreator} from '../../actions';
import {appStore} from '../../stores';

import AppHead from './head';
import AppBody from './body';
import {Home} from '../Page';
import {Container} from '../Elements';

const Application = React.createClass({
    mixins: [appStore.getMixin('application')],
    getInitialState(){
        let config = this.applicationStore.getConfig(CONFIG);
        let baseTitle = config['application-title'] || TITLE;
        let lang = config['language'] || LANGUAGE;
        let meta = [];
        let menu = {};
        let title = 'Loading...';

        if(_.isArray(config.meta)){
            meta = config.meta;
        }
        if(_.isObject(config.menu)){
            menu = config.menu;
        }

        return {
            baseTitle, lang,
            menu, meta, title,
            displayLog: _.get(this.props, 'location.query.debug') == 'show'
        };
    },
    componentDidMount() {
        let storeTitle = this.applicationStore.getTitle();
        let title = storeTitle ? storeTitle : null;
        appActionCreator.setPageTitle({title});
    },
    applicationStoreChanged(){
        let baseTitle = this.state.baseTitle;
        let storeTitle = this.applicationStore.getTitle();
        let title = storeTitle ? [baseTitle, storeTitle].join (' - ') : baseTitle;
        let log = this.applicationStore.getLog();
        this.setState({title, log});
    },
    componentWillReceiveProps(nextProps){
        let debug = _.get(this.props.location, 'query.debug');
        let nextDebug = _.get(nextProps.location, 'query.debug');
        if(debug !== nextDebug){
            this.toggleLogVisible(nextDebug == 'show');
        }
    },
    toggleLogVisible(isVisible = null){
        if(_.isNull(isVisible)){
            let displayLog = !this.state.displayLog;
            this.setState({displayLog});
        }else{
            let displayLog = !!isVisible;
            this.setState({displayLog});
        }
    },
    render() {
        return (
            <AppBody brand={config['application-brand']} 
                menu={this.state.menu}
                logging={!!this.state.log}
                location={this.props.location}>
                <AppHead
                    metaTags={this.state.meta}
                    docTitle={this.state.title}/>
                {this.props.children ? this.props.children : (
                    <Home/>
                )}
                {this.state.log ? (
                    <div className={classnames(
                        'log-area', {
                            'log-visible': !!this.state.displayLog
                        })}>
                        <Container>
                        <Panel header="Action log"
                            bsStyle="info">
                            <table className="table">
                            <thead>
                                <tr>
                                    <th>
                                        Time
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                    <th>
                                        ActionSource
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            {_.map(this.state.log, (entry, index) => (
                                <tr key={index}>
                                {_.map(entry, (field, key) => (
                                    <td key={key}
                                        className={key}>
                                        {!_.isString(field) ? JSON.stringify(field) : field}
                                    </td>
                                ))}
                                </tr>
                            ))}
                            </tbody>
                            </table>
                        </Panel>
                        </Container>
                    </div>
                ) : false}
            </AppBody>
        );
    }
});

export default Application;
