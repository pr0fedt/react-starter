'use strict';
import {partial} from 'lodash';
import React from 'react';

import invariant from './lib/invariant';

import {applicationActions} from './actions';
import {applicationStore, configurationStore} from './stores'
import {CONFIG, E_LOG_ACTION} from './constants';

import modules from './modules';
/** 
 *@dev:begin
 */
import Dispatcher from 'flux-extended/dist/_dispatcherSingleton';
Dispatcher.instance.logger = info => 
	(info.action.actionType != E_LOG_ACTION) ? 
	applicationActions.logAction(info) : true;
/** 
 *@dev:end
 */

const Main = React.createClass({
	mixins: [
		applicationStore.getMixin('app'),
		configurationStore.getMixin('config')
	],
	appStoreChanged(){
		let nextModule = this.appStore.nextModule();
		applicationActions.requestConfig({name: nextModule});
	},
	configStoreChanged(){

	},
	componentDidMount(){
		applicationActions.startApplication({
			modules: ['application']
		});
	},
	render(){
		return (
			<p>Loading...</p>
		);
	}
});
React.render(<Main/>, document.body);