'use strict';
import babelify from 'babelify';
import browserify from 'browserify';
import buffer from 'vinyl-buffer';
import gulp from 'gulp';
import gutil from 'gulp-util';
import fs from 'fs';
import less from 'gulp-less';
import lstream from 'length-stream';
import minimist from 'minimist';
import source from 'vinyl-source-stream';
import sourcemaps from 'gulp-sourcemaps';
import uglify from 'gulp-uglify';

import {GulpProgress} from './lib/gulp';
import {ServerFactory} from './dev-tools';

const KNOWN_OPTIONS = {
    string: ['port', 'host'],
    default: {
        port: '8000',
        host: '127.0.0.1'
    }
};

let cwd = process.cwd();
let options = minimist(process.argv.slice(2), KNOWN_OPTIONS);

gulp.task('pack', ['pack:client:less', 'pack:client:js']);

gulp.task('pack:client:less', () => 
    gutil.log(gutil.colors.blue('Compiling LESS Files...')) &&
    gulp.src(`${cwd}/styles/index.less`)
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest(`${cwd}/public/_build`))
);

gulp.task('pack:client:js', () => {
    let b = browserify({
        entries: `${cwd}/src/index.js`,
        debug: true,
        transform: [babelify]
    });

    gutil.log(gutil.colors.blue('Compiling JS Files...'));

    let Bar = new GulpProgress({format: 'Completed [:bar] :percent'});
    return b.bundle()
        .on('error', errorLog)
        .pipe(Bar.getStream(20, {readableObjectMode: true, writableObjectMode: true}))
        .pipe(source('index.js'))
        .pipe(Bar.getStream(20, {readableObjectMode: true, writableObjectMode: true}))
        .pipe(buffer())
        .pipe(Bar.getStream(20, {readableObjectMode: true, writableObjectMode: true}))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(Bar.getStream(20, {readableObjectMode: true, writableObjectMode: true}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(`${cwd}/public/_build`)).on('end', () => Bar.complete());

});

gulp.task('watch', ['pack'], () => 
    gulp.watch([`${cwd}/src/**/*.js`], ['pack:client:js']) &&
    gulp.watch([`${cwd}/styles/**/*.less`], ['pack:client:less'])
    );

gulp.task('dev-server', ['watch'], (done) => ServerFactory()({
    host: options.host,
    port: options.port,
    rootDir: cwd
}).then((server) => {
    let {address, port} = server.address();
    gutil.log(gutil.colors.green(`Development server started at https://${address}:${port}`));
}, (error) => done(error)));

function errorLog(error) {
    gutil.log(gutil.colors.green('******  Start of Error  ******'));
    gutil.log(gutil.colors.red(error));
    gutil.log(gutil.colors.red(error.stack));
    gutil.log(gutil.colors.green('******  End of Error  ******'));
    this.emit('end');
}
