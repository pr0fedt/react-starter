'use strict';
import fs from 'fs';
import http from 'http';
import path from 'path';
import _ from 'lodash';
import koa from 'koa'
import koaStatic from 'koa-smart-static';

import {parseParams} from '../lib/util';

const PUBLIC_URL = 'public';
const PUBLIC_DIR = 'public';
const INDEX_HTML = 'index.html';

function server({host, port, rootDir}){
    let html = '';
    const htmlStream = fs.createReadStream(path.join(rootDir, INDEX_HTML));
    return new Promise((resolve, reject) => htmlStream.on('data', (chunk) => html += chunk)
        .on('error', () => reject('index file could not be read'))
        .on('end', () => {
            const app = koa();
            app.use(koaStatic(path.join(rootDir, PUBLIC_DIR), PUBLIC_URL));
            app.use(function *(){
                this.body = html;
            });
            const koaServer = http.createServer(app.callback()).listen(port, host, () => resolve(koaServer));
        })
    );
}
export function ServerFactory(...args){
    let param = parseParams(args, '!host@localhost', '!port@8000', 'rootDir');
    return function createServer(options = {}){
        let {host, port, rootDir} = options;
        host = host || param.host;
        port = parseInt(port || param.port);
        rootDir = rootDir || param.rootDir;
        if(!_.isString(rootDir)){
            throw new Error(
`Dear sir! Do not be so forgetful.
May I please have at least something to know the path.`
            );
        }
        return server({port, host, rootDir});
    }
}
