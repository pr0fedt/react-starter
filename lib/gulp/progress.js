'use strict';
import util from 'util';
import {Transform as TransformStream} from 'stream';
import LengthStream from 'length-stream';
import Bar from 'progress';
let Mod = 0.005;
const DEFAULT_BAR_FORMAT = 'progress [:bar]';
const TOTAL = 100.0;

GulpProgress.changeModifier = function(mod){
    Mod = mod;
}

function GulpProgress(barOptions){
    barOptions.total = TOTAL;
    this.bar = new Bar(barOptions.format || DEFAULT_BAR_FORMAT, barOptions);
    this.limit = 0.0;
    Object.defineProperties(this, {
        curr: {
            get: () => this.bar.curr
        },
        leftLimit: {
            get: () => {
                let res = parseFloat(this.limit - this.bar.curr);
                return res < 0.0 ? 0.0 : res;
            }
        },
        leftTotal: {
            get: () => {
                let res = parseFloat(this.bar.total - this.bar.curr);
                return res < 0.0 ? 0.0 : res;
            }
        }
    });
}
GulpProgress.prototype.incLimit = function(val){
    val = Math.abs(val);
    if(this.limit + val >= this.bar.total){
        this.limit = this.bar.total;
    }else{
        this.limit += val;
    }
    return this;
}
GulpProgress.prototype.incLimitMax = function(){
    this.incLimit(this.bar.total);
}
GulpProgress.prototype.inc = function(val){
    val = parseFloat(val);
    let modifier = val > this.leftLimit ? val - this.leftLimit : val;
    this.bar.tick(Math.abs(modifier));
    return this;
};
GulpProgress.prototype.incMax = function(){
    this.inc(this.bar.total);
    return this;
};
GulpProgress.prototype.incRandom = function(multiplier){
    multiplier = parseFloat(multiplier);
    let modifier = Math.random() * this.leftLimit;
    if(multiplier >= 0.0 && multiplier <= 1.0){
        this.inc(modifier * multiplier);
    }
    return this;
};
GulpProgress.prototype.complete = function(){
    this.incLimitMax();
    this.incMax();
};
GulpProgress.prototype.getStream = function(limit, options = {}){
    this.incLimit(limit);

    options.limit = limit;
    options.progress = this;

    return new ProgressStream(options);
};

util.inherits(ProgressStream, TransformStream);
function ProgressStream(options){
    TransformStream.call(this, options);
    this.limit = options.limit;
    this.progressBar = options.progress;
};
ProgressStream.prototype._transform = function(data, enc, next){
    this.progressBar.incRandom(Mod);
    this.push(data);
    next();
};
ProgressStream.prototype._flush = function(done){
    this.progressBar.inc(this.limit);
    this.push(null);
    done();
};

export {GulpProgress};