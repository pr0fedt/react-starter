'use strict';
const REQUIRED_SYMBOL = '!';
const DEFAULT_SYMBOL  = '@';

export function parseParams(src, ...param){
    let result = {};

    for(let i=0; i < param.length; ++i){
        let dIndex = -1;
        let dValue = null;

        let required = false;
        let rIndex = param[i].indexOf(REQUIRED_SYMBOL);

        let pKey;

        if(rIndex >= 0){
            dIndex = param[i].indexOf(DEFAULT_SYMBOL);
            required = true;
            if(dIndex > rIndex){
                dValue = param[i].slice(dIndex + 1);
                pKey = param[i].slice(0, rIndex) +
                    param[i].slice(rIndex + 1, dIndex);
            }else if(dValue < 0){
                pKey = param[i].slice(0, rIndex) +
                    param[i].slice(rIndex + 1, param[i].length);
            }else{
                throw new Error('incorrect symbol in parameter description')
            }
        }else{
            pKey = param[i];
        }

        if(src[i]){
            result[pKey] = src[i];
        } else if(dValue !== null){
            result[pKey] = dValue;
        } else if(required){
            throw new Error(`missing ${i}th required parameter [${pKey}]`);
        }
    }
    return result;
};
