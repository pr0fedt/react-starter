# Starter reactjs frontend project template #
- Setup
```bash
npm install
```
- Run dev server
```bash
npm start -- [--port PORTNO] [--host HOSTNAME]
```